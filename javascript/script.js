alert("Welcome to Maths Tester Pro");
let setDiff;
let lives;
let max_num;
let min_num;
let questions;
let score = 0;

let rand1;
let rand2;
let rand3;
let operand;
let question;
let answer;
let userAnswer;
let percentage;
let grade;

let easymode = false;
let mediummode = false;
let hardmode = false;


for(i=0;i<100;i++) {
setDiff = prompt(`Select a difficulty: 1)Easy 2)Medium 3)Hard`);
if(setDiff == 1 || setDiff == "e" || setDiff == "easy") {
    easymode = true;
    lives = 3;
    min_num = 5;
    max_num = 10;
    questions = 5;
    alert("Easy mode selected.")
    break
}
else if(setDiff == 2 || setDiff == "m" || setDiff == "medium") {
    mediummode = true;
    lives = 2;
    min_num = 12;
    max_num = 25;
    questions = 10;
    alert("Medium mode selected.")
    break
}
else if(setDiff == 3 || setDiff == "h" || setDiff == "hard") {
    hardmode = true;
    lives = 1;
    min_num = 25;
    max_num = 50;
    questions = 15;
    alert("Hard mode selected.")
    break
}
else {
    alert("Not a valid answer")
}
}


for(j=0;j<50;j++) {
for(i=0;i<questions;i++) { 
    if(lives > 1) {
        alert(`Question ${i+1} of ${questions}. You have ${lives} lives remaining`)
    }
    else if(lives == 1) {
        alert(`Question ${i+1} of ${questions}. You have only one life remaining`)
    }
    else {
        alert("Out of lives! Game Over...")
        break
    }
    if(i == (questions-1)) {
        alert("Challenge question!")
        ask_question((min_num*2),(max_num*2))
    }
    else {
        ask_question(min_num, max_num)
    }
}

alert("Test Complete")
percentage = Math.round(score / questions * 100);
alert(`You scored ${score}/${questions}(${percentage}%)`)
if(percentage < 50) {
    grade = "Fail"
}
else if(percentage < 60 && percentage >= 50) {
    grade = "Pass"
}
else if(percentage < 70 && percentage >= 60) {
    grade = "Credit"
}
else if(percentage < 80 && percentage >= 70) {
    grade = "Distinction"
}
else if(percentage < 101 && percentage >= 80) {
    grade = "High Distinction"
}

alert(`Your grade is ${grade}`)
choice = prompt("Do you wish to go again?(y for Yes || n for No")
if(choice == "y" || choice == "yes") {
    score = 0;
    if(easymode == true) {
        lives = 3;
    }
    else if(mediummode == true) {
        lives = 2;
    }
    else if(hardmode == true) {
        lives = 1;
    }
    else {
        alert("Invalid option")
    }
}
else if (choice == "n" || choice == "no") {
    break
}
}





function ask_question(minimum, maximum) {
        rand1 = Math.floor((Math.random() * maximum) + 1);
        rand2 = Math.floor((Math.random() * maximum) + 1);
        rand3 = Math.floor((Math.random() * 2) + 1);

        if(rand3 == 1) {
            operand = "-"
            answer = rand1 - rand2;
        }
        else {
            operand = "+"
            answer = rand1 + rand2;
        }
        
        question = rand1 + operand + rand2;
        userAnswer = parseInt(prompt(`What is ${question} ?`));
        
        if(userAnswer == answer) {
            alert("That is correct!")
            score += 1;
        }
        else {
            alert(`That is incorrect! The correct answer is ${answer}`)
            lives -= 1;
        }
    }




